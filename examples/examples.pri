win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../lib -lqfacebook
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../lib -lqfacebook
else:symbian: LIBS += -lqfacebook
else:unix: LIBS += -L$$OUT_PWD/../../lib -lqfacebook

INCLUDEPATH += $$PWD/../src
DEPENDPATH += $$PWD/../src

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/../../src/3rdparty/qjson/lib -lqjson
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/../../src/3rdparty/qjson/lib -lqjson
else:symbian: LIBS += -lqjson
else:unix: LIBS += -L$$OUT_PWD/../../src/3rdparty/qjson/lib -lqjson

INCLUDEPATH += $$PWD/../../src/3rdparty/qjson/include
DEPENDPATH += $$PWD/../../src/3rdparty/qjson/src
