QT -= gui
QT += network

TARGET = qfacebook
TEMPLATE = lib
DESTDIR  = ../lib

DEFINES += QFACEBOOK_LIBRARY

win32:CONFIG(release, debug|release): LIBS += -L$$OUT_PWD/3rdparty/qjson/lib -lqjson
else:win32:CONFIG(debug, debug|release): LIBS += -L$$OUT_PWD/3rdparty/qjson/lib -lqjson
else:symbian: LIBS += -lqjson
else:unix: LIBS += -L$$OUT_PWD/3rdparty/qjson/lib -lqjson

INCLUDEPATH += $$PWD/3rdparty/qjson/include
DEPENDPATH += $$PWD/3rdparty/qjson/src

SOURCES += \
    qfacebook.cpp \
    graphapi.cpp \
    qfacebookreply.cpp

HEADERS += \
    qfacebook.h \
    qfacebook_global.h \
    graphapi.h \
    qfacebookreply.h


headers.files = qfacebook.h qfacebookreply.h

symbian {
    MMP_RULES += EXPORTUNFROZEN
    TARGET.UID3 = 0xE106BC58
    TARGET.CAPABILITY =
    TARGET.EPOCALLOWDLLDATA = 1
    addFiles.sources = qfacebook.dll
    addFiles.path = !:/sys/bin
    DEPLOYMENT += addFiles
}

unix:!symbian {
    maemo5 {
        target.path = /opt/usr/lib
    } else {
        target.path = /usr/lib
    }
    INSTALLS += target headers
}
